import { randomAddDivider } from "../../constants/random";

export function randomBetween(min, max) {
  const multiplier = max - min;
  const addRandom = (Math.random() / randomAddDivider) - (Math.random() / randomAddDivider);
  const multipliedRandom = Math.random() * multiplier;
  const composedRandom = min + addRandom + multipliedRandom;
  const maxTreshold = Math.min(max, composedRandom);
  const minTreshold = Math.max(min, maxTreshold);

  return minTreshold;
}
