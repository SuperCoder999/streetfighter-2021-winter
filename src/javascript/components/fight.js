import { controls } from '../../constants/controls';
import { criticalHitIntervalMs } from '../../constants/timing';
import { fighterDetailsToBattleFighter } from '../helpers/fighterHelper';
import { randomBetween } from '../helpers/randomHelper';

export function fight(firstFighter, secondFighter) {
  const firstIndicator = document.getElementById('left-fighter-indicator');
  const secondIndicator = document.getElementById('right-fighter-indicator');

  const first = fighterDetailsToBattleFighter(firstFighter);
  const second = fighterDetailsToBattleFighter(secondFighter);

  const pressedKeys = new Set();

  return new Promise((resolve) => {
    const handleKeyDown = (event) => {
      const key = event.code;
      pressedKeys.add(key);

      handleBlocks(pressedKeys, first, second);
      handleAttacks({ pressedKeys, first, second, firstIndicator, secondIndicator });
      handleCriticalHits({ pressedKeys, first, second, firstIndicator, secondIndicator });

      const winner = getWinner(first, second);

      if (winner) {
        document.body.removeEventListener('keydown', handleKeyDown);
        document.body.removeEventListener('keyup', handleKeyUp);

        resolve(winner);
      }
    };
    
    const handleKeyUp = (event) => {
      const key = event.code;
      pressedKeys.delete(key);
    };

    document.body.addEventListener('keydown', handleKeyDown);
    document.body.addEventListener('keyup', handleKeyUp);
  });
}

export function handleAttacks({ pressedKeys, first, second, firstIndicator, secondIndicator }) {
  const canBothAttack = canAttack(first, second);
  const firstAttacks = pressedKeys.has(controls.PlayerOneAttack) && canBothAttack;
  const secondAttacks = pressedKeys.has(controls.PlayerTwoAttack) && canBothAttack;

  if (firstAttacks) {
    doAttackDamage(first, second, secondIndicator);
  }

  if (secondAttacks) {
    doAttackDamage(second, first, firstIndicator);
  }
}

export function handleBlocks(pressedKeys, first, second) {
  first.blocked = pressedKeys.has(controls.PlayerOneBlock);
  second.blocked = pressedKeys.has(controls.PlayerTwoBlock);
}

export function handleCriticalHits({ pressedKeys, first, second, firstIndicator, secondIndicator }) {
  const firstAttacks =
    controls.PlayerOneCriticalHitCombination.every((k) => pressedKeys.has(k)) &&
    canCriticalHit(first);

  const secondAttacks =
    controls.PlayerTwoCriticalHitCombination.every((k) => pressedKeys.has(k)) &&
    canCriticalHit(second);

  if (firstAttacks) {
    doCriticalHitDamage(first, second, secondIndicator);
  }

  if (secondAttacks) {
    doCriticalHitDamage(second, first, firstIndicator);
  }
}

export function doAttackDamage(attacker, defender, defenderHealthIndicator) {
  defender.health -= getDamage(attacker, defender);
  setIndicatorHealth(defender, defenderHealthIndicator);
}

export function doCriticalHitDamage(attacker, defender, defenderHealthIndicator) {
  defender.health -= getCriticalHitPower(attacker);
  attacker.lastCriticalHitMs = new Date().getTime();

  setIndicatorHealth(defender, defenderHealthIndicator);
}

export function getWinner(first, second) {
  if (first.health <= 0) {
    return second;
  }

  if (second.health <= 0) {
    return first;
  }
}

export function getDamage(attacker, defender) {
  const difference = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, difference);
}

export function getHitPower(fighter) {
  return fighter.attack * getCriticalChance();
}

export function getCriticalHitPower(fighter) {
  return fighter.attack * 2;
}

export function getBlockPower(fighter) {
  return fighter.defense * getCriticalChance();
}

export function getCriticalChance() {
  return randomBetween(1, 2);
}

export function canAttack(attacker, defender) {
  return !(attacker.blocked || defender.blocked);
}

export function canCriticalHit(fighter) {
  const currentTime = new Date().getTime();
  const lastPossibleHitTime = currentTime - criticalHitIntervalMs;

  return fighter.lastCriticalHitMs <= lastPossibleHitTime && !fighter.blocked;
}

export function setIndicatorHealth(fighter, indicator) {
  const percentage = fighter.health / fighter.maxHealth * 100;
  const nonNegativePercentage = Math.max(0, percentage);

  indicator.style.width = `${nonNegativePercentage}%`;
}
