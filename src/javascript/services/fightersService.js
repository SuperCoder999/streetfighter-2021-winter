import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    const endpoint = 'fighters.json';
    return await callApi(endpoint);
  }

  async getFighterDetails(id) {
    const endpoint = `details/fighter/${id}.json`;
    return await callApi(endpoint);
  }
}

export const fighterService = new FighterService();
