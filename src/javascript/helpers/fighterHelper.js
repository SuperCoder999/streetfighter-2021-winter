import { criticalHitIntervalMs } from '../../constants/timing';

export function fighterDetailsToBattleFighter(details) {
  return {
    ...details,
    maxHealth: details.health,
    lastCriticalHitMs: -criticalHitIntervalMs,
    blocked: false,
  };
}
