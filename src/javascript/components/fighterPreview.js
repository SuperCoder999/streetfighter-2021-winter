import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';

  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!fighter) {
    return fighterElement;
  }

  const imageElement = createFighterImage(fighter);
  const nameElement = createFighterName(fighter.name);
  const healthElement = createFighterNumericProperty('Health', fighter.health);
  const attackElement = createFighterNumericProperty('Attack', fighter.attack);
  const defenseElement = createFighterNumericProperty('Defense', fighter.defense);

  fighterElement.append(nameElement, healthElement, attackElement, defenseElement, imageElement);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;

  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };

  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterNumericProperty(propertyName, number) {
  const element = createElement({
    tagName: 'div',
    className: 'fighter-preview___number',
  });

  element.innerText = `${propertyName}: ${number}`;
  return element;
}

export function createFighterName(name) {
  const element = createElement({
    tagName: 'div',
    className: 'fighter-preview___name'
  });

  element.innerText = name;
  return element;
}
