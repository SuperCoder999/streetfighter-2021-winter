import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const bodyElement = createElement({ tagName: 'div' });
  bodyElement.innerText = `The winner is ${fighter.name}!`;

  showModal({
    title: 'Game over!',
    bodyElement,
    onClose: () => window.location.reload(),
  });
}
